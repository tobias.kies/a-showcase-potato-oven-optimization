import pytest

from oven_optimizer.configuration_manager import Configuration


@pytest.fixture
def sample_data():
    return {
        'sample_key': 'sample_value',
        'nested': {
            'sample_key': 'nested_value',
            'another_key': 'another_value'
        }
    }


class SampleObject:
    def __init__(self):
        self.sample_key = None


def test_configuration_as_dict(sample_data):
    configuration = Configuration(sample_data)
    assert sample_data == configuration.as_dict()


def test_configuration_get_present(sample_data):
    configuration = Configuration(sample_data)
    assert configuration.get('sample_key') == 'sample_value'


def test_configuration_get_nested(sample_data):
    configuration = Configuration(sample_data)
    assert configuration.get('nested/sample_key') == 'nested_value'


def test_configuration_get_not_present(sample_data):
    configuration = Configuration(sample_data)
    assert configuration.get('not_sample_key', 'not_sample_value') == 'not_sample_value'


def test_configuration_get_from_attribute_error(sample_data):
    configuration = Configuration(sample_data)
    assert configuration.get('sample_key/sub', 'not_sample_value') == 'not_sample_value'


def test_configuration_initialize_object(sample_data):
    configuration = Configuration(sample_data)
    sample_object = configuration.initialize_object(SampleObject, 'nested')
    assert sample_object.sample_key == 'nested_value'
