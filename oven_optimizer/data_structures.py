''' This file contains the discrete system data structure.
'''
import numpy as np


class DiscreteSystems:
    def __init__(self):
        '''This class holds a collection of discretization matrices and
        vectors that are relevant throughout this example.
        Effectively, we could also do this by using a dictionary.
        However, this approach allows us to document the relevant entities
        more explicitly.
        '''
        # Mass matrix M
        self.mass_matrix = None

        # Mass matrix M_B over the subdomain B
        self.subdomain_mass_matrix = None

        # Mass matrix M_B applied to the constant 1-function
        # and normalized by the domain size of B.
        self.subdomain_unit_mass_vector = None

        # Laplace Matrix A
        self.laplace_matrix = None

        # Boundary mass Matrix M_\Gamma
        self.boundary_mass_matrix = None

        # Boundary mass matrix M_\Gamma applied to the constant 1-function
        self.boundary_unit_mass_vector = None

        # Constant one function.
        self.constant_one = None

        # List of indices of degrees of freedom on the boundary.
        self.boundary_dof_indices = None

    def get_potato_temperature(self, temperatures):
        return np.dot(self.subdomain_unit_mass_vector, temperatures)

    def get_energy_consumption(self, times, controls):
        ''' Calculate the cumulative integral over the domain boundary. '''
        time_deltas = np.diff(times)
        integrals = [0]
        for time_delta, control in zip(time_deltas, controls[: len(time_deltas)]):
            integrals.append(
                integrals[-1]
                + time_delta
                * np.dot(control, np.matmul(self.boundary_mass_matrix, control))
            )
        return integrals

    def embed_from_boundary(self, boundary_coefficients):
        '''Turns an array of boundary-coefficients to an array of
        volume coefficients.'''
        volume_coefficients = np.zeros_like(self.constant_one.coefficients)
        volume_coefficients[self.boundary_dof_indices] = boundary_coefficients
        return volume_coefficients

    def reduce_to_boundary(self, volume_coefficients):
        '''Extracts from array of volume coefficients all coefficients
        that are associated to the domain's boundary.'''
        boundary_coefficients = volume_coefficients[self.boundary_dof_indices]
        return boundary_coefficients
