import numpy as np

from .local_logging import logger


class ProjectedGradientMethod:
    def __init__(self, max_num_iterations=10, store_history=False):
        ''' Projected gradient method for convex problems. '''
        self.max_num_iterations = max_num_iterations
        self.store_history = store_history
        self._history = None
        self._last_step_size = 1
        self._current_value = None
        self._armijo_constant = 1e-4
        self._num_iterations = None

    def run(self, function, derivative, projection, initial_iterate):
        iterate = initial_iterate
        if self.store_history:
            self._history = [iterate]
        self._current_value = function(iterate)
        for iteration_id in range(self.max_num_iterations):
            logger.debug(
                'Projected gradient method at iteration %d' ' with value %.2f',
                iteration_id,
                self._current_value,
            )
            self._num_iterations = iteration_id
            gradient = derivative(iterate)
            gradient_norm = np.linalg.norm(gradient, np.inf)
            search_direction = -gradient

            projection_direction = projection(iterate - gradient) - iterate
            projection_norm = np.linalg.norm(projection_direction, np.inf)
            logger.debug(
                'Projection inf norm: %.2f, Gradient inf norm: %.2f',
                projection_norm,
                gradient_norm,
            )
            if projection_norm < 1e-10:
                logger.info('Stopping gradient method: convegence')
                break

            if iteration_id == 0:
                # Heuristic for faster step size finding.
                self._last_step_size = np.linalg.norm(
                    projection_direction
                ) / np.linalg.norm(gradient)

            step_size = self._get_armijo_step_size(
                function, iterate, gradient, search_direction, projection
            )
            if step_size == 0:
                logger.info('Stopping gradient method: no step size found')
                break

            iterate = projection(iterate + step_size * search_direction)
            if self.store_history:
                self._history.append(iterate)
            if iteration_id + 1 == self.max_num_iterations:
                logger.info('Stopping gradient method: maxiter exceeded')
        return iterate

    def get_history(self):
        return self._history

    def get_num_iterations(self):
        return self._num_iterations

    def _get_armijo_step_size(
        self, function, iterate, gradient, search_direction, projection
    ):
        step_size = min(4 * self._last_step_size, 2)
        old_value = (
            self._current_value
            if self._current_value is not None
            else function(iterate)
        )
        descent_product = self._armijo_constant * np.dot(
            gradient.flatten(), search_direction.flatten()
        )
        assert descent_product < 0
        while step_size > 1e-15:
            step_size *= 0.5
            new_iterate = projection(iterate + step_size * search_direction)
            new_value = function(new_iterate)
            logger.debug(
                'Trying armijo step %s yielding value %s (vs %s)',
                step_size,
                new_value,
                old_value,
            )
            if new_value <= old_value + step_size * descent_product:
                break
            if step_size < 1e-15:
                return 0
        self._current_value = new_value
        self._last_step_size = step_size
        return step_size
