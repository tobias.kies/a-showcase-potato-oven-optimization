import logging
import os


def get_logging_level():
    debug_level = os.getenv('LOGGING_LEVEL', 'debug')
    if debug_level == 'info':
        return logging.INFO
    elif debug_level == 'error':
        return logging.ERROR
    return logging.DEBUG


def init_logger(name):
    ''' Default initialize a local logger. '''
    logger = logging.getLogger(name)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.handlers = []
    logger.propagate = False
    logger.addHandler(console_handler)
    logger.setLevel(get_logging_level())
    return logger


logger = init_logger(__file__)
