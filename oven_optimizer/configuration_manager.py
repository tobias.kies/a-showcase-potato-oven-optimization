import json

import yaml


class Configuration:
    def __init__(self, configuration):
        '''This class is a simple storage container for configuration data.

        Attributes
        ----------
        configuration : dict-like
            Configuration data that is to be managed.
        '''
        self._configuration = configuration
        self._key_delimiter = '/'

    def as_dict(self):
        return self._configuration

    def get(self, key, default_value=None):
        '''Get
        Attributes
        ----------
        key : str
            Identifier of the configuration setting that is to be accessed.
            Nested hierarchies can be specified by concatenating multiple
            keys with the key_delimiter '/'.

        default_value : object, optional
            The default value to return in case the specified key is not
            found. By default, this is set to None.

        Returns
        -------
        object
            The value at the specified key, if present, else the
            `default_value`.
        '''
        item = self._configuration
        for sub_key in key.split(self._key_delimiter):
            try:
                item = item.get(sub_key, default_value)
            except AttributeError:
                item = default_value
                break
        return item

    def initialize_object(self, cls, key):
        '''Initialize an object from the configuration. This is only designed
        for very simple objects which can be null-constructed and
        initialized via basic attribute assignments.

        Attributes
        ----------
        cls : class
            Class of the object that is to be initialized.
            It needs to support empty initialization `x = cls()` and all
            relevant attributes need to be accessible as x.attribute = y'.

        key : str
            The key at which the relevant configuration is to be found.

        Returns
        -------
        cls
            The initialized object of type `cls`.
        '''
        data = self.get(key)
        obj = cls()
        try:
            for attribute_name, value in data.items():
                try:
                    setattr(obj, attribute_name, value)
                except AttributeError:
                    pass
        except AttributeError:
            pass
        return obj


def read(file_path):
    '''Read a configuration object from a file path.

    Attributes
    ----------
    file_path : str
        Path to the location of the configuration file.
        Currently, *.json, *.yml and *.yaml files are supported.

    Returns
    -------
    Configuration
        Configuration object containing the file's data.

    Raises
    ------
    ValueError
        If `file_path` has an invalid extension.
    '''
    extension = file_path.split('.')[-1].lower()
    if extension not in ['json', 'yml', 'yaml']:
        raise ValueError(f'Invalid extension: {extension}')
    with open(file_path, 'r') as file:
        if extension == 'json':
            data = json.load(file)
        elif extension in ['yml', 'yaml']:
            data = yaml.safe_load(file)
        else:
            data = None
    return Configuration(data)
