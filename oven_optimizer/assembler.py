import fesuite
import numpy as np

from oven_optimizer.local_logging import logger

from .data_structures import DiscreteSystems


def assemble_all_discrete_systems(config):
    ''' Convenience function for assembling all problem relevant system
        matrices and vectors from a given problem configuration.
        This is possible because the general problem under consideration
        is linear.
    '''
    # Initialize data placeholder.
    discrete_systems = DiscreteSystems()

    # Set up the grid and the finite element space.
    grid = make_grid(config)
    reference_function = fesuite.make_hermite_function(
        dim=2,
        order=1,
        vecs=grid
    )

    # Set the constant-one function.
    discrete_systems.constant_one = make_constant_function(
        reference_function,
        constant=1
    )

    # Assemble the mass matrix.
    discrete_systems.mass_matrix = \
        fesuite.assemble_mass_matrix(reference_function)
    logger.debug('Got mass matrix.')

    # Assemble the mass matrix that is restricted to the domain B.
    discrete_systems.subdomain_mass_matrix = \
        fesuite.assemble_circle_mass_matrix(
            reference_function,
            center=[0, 0],
            radius=config.get('problem/potato_radius')
        )
    logger.debug('Got subdomain mass matrix.')

    # Assemble the "constant one over volume of subdomain B" vector
    # and normalize it by the area of the domain B.
    discrete_systems.subdomain_unit_mass_vector = \
        np.matmul(
            discrete_systems.subdomain_mass_matrix,
            discrete_systems.constant_one.coefficients
        ) / (config.get('problem/potato_radius')**2 * np.pi)
    logger.debug('Got subdomain unit mass vector.')

    # Assemble the laplace system matrix.
    discrete_systems.laplace_matrix = config.get('problem/diffusivity') \
        * fesuite.assemble_laplace_matrix(reference_function)
    logger.debug('Got laplace matrix.')

    # Assemble the mass matrix along the boundary.
    discrete_systems.boundary_mass_matrix = \
        fesuite.assemble_boundary_mass_matrix(reference_function)
    logger.debug('Got boundary mass matrix.')

    # Assemble the "constant one mass along boundary" vector.
    discrete_systems.boundary_unit_mass_vector = \
        np.matmul(
            discrete_systems.boundary_mass_matrix,
            discrete_systems.constant_one.coefficients
        )
    logger.debug('Got boundary unit mass vector.')

    # Get a list of all boundary-DOF indices.
    # This is a quick solution which exploits the specific
    # sturcture of the given finite element space.
    discrete_systems.boundary_dof_indices = \
        [i for i, b in enumerate(discrete_systems.boundary_unit_mass_vector)
         if b != 0]

    return discrete_systems


def make_grid(config):
    width = config.get('discretization/width') / 2
    height = config.get('discretization/height') / 2
    num_width_nodes = config.get('discretization/num_width_nodes')
    num_height_nodes = config.get('discretization/num_height_nodes')
    grid = [
        np.linspace(-width, width, num_width_nodes),
        np.linspace(-height, height, num_height_nodes)
    ]
    logger.debug(
        'Made grid with dimensions %s by %s, using %s by %s nodes.',
        width * 2,
        height * 2,
        num_width_nodes,
        num_height_nodes
    )
    return grid


def make_constant_function(function_space, constant=1):
    ''' For a given hermite function space, this interpolates a constant
        function.

        Note: The interpolation is somewhat harcoded here.
              It would be better if the fesuite library could offer
              a proper interpolation routine all by itself.
    '''
    dim = function_space.dim
    order = function_space.order
    grid = function_space.grid
    values = np.zeros_like(function_space.coefficients)

    dofs_per_node = order**dim
    indices = [index % dofs_per_node == 0 for index in range(len(values))]
    values[indices] = constant
    constant_function = fesuite.make_hermite_function(dim, order, grid)
    for i, value in enumerate(values):
        constant_function.coefficients[i] = value
    return constant_function
