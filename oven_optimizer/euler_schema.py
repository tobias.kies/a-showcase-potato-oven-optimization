''' Euler schemas for time-dependent problems.

    To-Do: Could benefit from some refactoring and has potential for
           performance improvements.
'''

import numpy as np


class ImplicitEulerSchema:
    def __init__(self, time_step, mass_matrix, system_matrix, right_hand_side):
        '''Specialization of the implicit Euler schema for linear systems.
        Also supports moving backwards in time.
        '''
        self._time_step = time_step
        self._time_sign = 1 if time_step > 0 else -1
        self._mass_matrix = mass_matrix
        self._system_matrix = system_matrix
        self._left_hand_system = mass_matrix - time_step * system_matrix
        self._right_hand_side_factory = right_hand_side

    def run(
        self, initial_iterate, stop_time, return_only_last_iterate=True, start_time=0
    ):
        iterate = initial_iterate
        if not return_only_last_iterate:
            iterates_history = [initial_iterate]
        time = start_time
        assert (stop_time - start_time) * self._time_step >= 0
        times = [time]
        while (stop_time - time) * (time - start_time) >= 0:
            if time == stop_time:  # Special case: Spot landing.
                break
            time += self._time_step
            # print('euler norms', time, np.linalg.norm(iterate))
            if not np.isfinite(np.linalg.norm(iterate)):
                raise RuntimeError('Encountered non-finite value in Euler schema.')

            right_hand_side = np.matmul(
                self._mass_matrix, iterate
            ) + self._time_sign * self._right_hand_side_factory(iterate)
            # Remark: Perfomance could probably be improved significantly by
            #         using a better solver here.
            iterate = np.linalg.solve(self._left_hand_system, right_hand_side)

            if not return_only_last_iterate:
                times.append(time)
                iterates_history.append(iterate)
        if return_only_last_iterate:
            return time, iterate
        return times, iterates_history


class ExplicitEulerSchema:
    def __init__(self, time_step, mass_matrix, system_matrix, right_hand_side):
        '''Specialization of the explicit Euler schema for linear systems.
        Also supports moving backwards in time.
        '''
        self._time_step = time_step
        self._time_sign = 1 if time_step > 0 else -1
        self._mass_matrix = mass_matrix
        self._system_matrix = system_matrix
        self._right_hand_system = mass_matrix + time_step * system_matrix
        self._right_hand_side_factory = right_hand_side

    def run(
        self, initial_iterate, stop_time, return_only_last_iterate=True, start_time=0
    ):
        iterate = initial_iterate
        if not return_only_last_iterate:
            iterates_history = [initial_iterate]
        time = start_time
        assert (stop_time - start_time) * self._time_step >= 0
        times = [time]
        while (stop_time - time) * (time - start_time) >= 0:
            if time == stop_time:  # Special case: Spot landing.
                break
            time += self._time_step
            # print('explicit euler norms', time, np.linalg.norm(iterate))
            if not np.isfinite(np.linalg.norm(iterate)):
                raise RuntimeError

            right_hand_side = np.matmul(
                self._right_hand_system, iterate
            ) + self._time_sign * self._right_hand_side_factory(iterate)
            # Remark: Perfomance could probably be improved significantly by
            #         using a better solver here.
            iterate = np.linalg.solve(self._mass_matrix, right_hand_side)

            if not return_only_last_iterate:
                times.append(time)
                iterates_history.append(iterate)
        if return_only_last_iterate:
            return time, iterate
        return times, iterates_history
