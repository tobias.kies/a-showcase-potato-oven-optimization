import numpy as np

from .euler_schema import ImplicitEulerSchema, ExplicitEulerSchema
from .local_logging import logger


class HeatingControlProblemDiscretization:
    def __init__(
        self,
        initial_state,
        target_state,
        regularization,
        end_time,
        discrete_systems,
        time_step,
    ):
        self._initial_state = initial_state
        self._target_state = target_state
        self._regularization = regularization
        self._end_time = end_time

        self._discrete_systems = discrete_systems
        self._time_step = time_step

        self._last_control = None
        self._last_times = None
        self._last_adjoint = None
        self._last_state_trajectory = None
        self._last_final_state = None

    def get_cost(self, control):
        residual_cost = self._get_residual_cost(control)
        regularization_cost = self._get_regularization_cost(control)
        return residual_cost + regularization_cost

    def get_derivative(self, control):
        self._update(control, update_adjoint=True)
        return self._last_adjoint + self._regularization * control

    def get_last_state_trajectory(self):
        return self._last_times, self._last_state_trajectory

    def _get_residual_cost(self, control):
        self._update(control, update_adjoint=False)
        residual = self._get_residual()
        residual_cost = 0.5 * np.dot(
            residual, np.matmul(self._discrete_systems.mass_matrix, residual)
        )
        return residual_cost

    def _get_regularization_cost(self, control):
        regularization_cost = 0
        for i in range(np.shape(control)[0]):
            fixed_time_control = self._discrete_systems.embed_from_boundary(
                control[i, :]
            )
            regularization_cost += self._time_step * np.dot(
                fixed_time_control,
                np.matmul(
                    self._discrete_systems.boundary_mass_matrix, fixed_time_control
                ),
            )
        return 0.5 * self._regularization * regularization_cost

    def _update(self, control, update_adjoint=True):
        if self._last_control is None or not np.all(control == self._last_control):
            self._last_adjoint = None
            self._update_final_state(control)
        if self._last_adjoint is None and update_adjoint is True:
            self._update_adjoint(control)

    def _update_final_state(self, control):
        def right_hand_side_generator(state):
            i = right_hand_side_generator.counter
            embedded_control = self._discrete_systems.embed_from_boundary(control[i, :])
            right_hand_side = np.matmul(
                self._discrete_systems.boundary_mass_matrix, embedded_control
            )
            right_hand_side_generator.counter += 1
            return right_hand_side

        right_hand_side_generator.counter = 0
        euler_schema = ImplicitEulerSchema(
            self._time_step,
            self._discrete_systems.mass_matrix,
            -self._discrete_systems.laplace_matrix,
            right_hand_side_generator,
        )
        self._last_times, self._last_state_trajectory = euler_schema.run(
            initial_iterate=self._initial_state.coefficients,
            stop_time=self._end_time,
            return_only_last_iterate=False,
        )
        self._last_final_state = self._last_state_trajectory[-1]
        logger.info(
            'Current final state norm is %.2f',
            np.linalg.norm(self._last_final_state, 2),
        )

    def _update_adjoint(self, control):
        def right_hand_side_generator(state):
            return np.zeros_like(state)

        euler_schema = ExplicitEulerSchema(
            self._time_step,
            self._discrete_systems.mass_matrix,
            -self._discrete_systems.laplace_matrix,
            right_hand_side_generator,
        )
        logger.info(
            'Current residual norm is %.2f', np.linalg.norm(self._get_residual(), 2)
        )
        _, adjoints = euler_schema.run(
            initial_iterate=self._get_residual(),
            start_time=0,
            stop_time=self._end_time,
            return_only_last_iterate=False,
        )
        self._last_adjoint = np.array(
            [
                self._discrete_systems.reduce_to_boundary(adjoint)
                for adjoint in reversed(adjoints)
            ]
        )
        logger.info(
            'Current norm of adjoint is %.2f', np.linalg.norm(self._last_adjoint, 2)
        )

    def _get_residual(self):
        return self._last_final_state - self._target_state.coefficients
