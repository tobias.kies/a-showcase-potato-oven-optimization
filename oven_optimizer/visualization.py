import warnings

import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401

import numpy as np
import seaborn as sns

from .configuration_manager import Configuration
from .assembler import make_grid


def make_line_plot(x, y, title, path=None, x_label=None, y_label=None):
    plt.figure()
    ax = sns.lineplot(x=x, y=y)
    ax.set_title(title)
    if x_label is not None:
        ax.set_xlabel(x_label)
    if y_label is not None:
        ax.set_ylabel(y_label)
    if path is not None:
        plt.savefig(path)


def make_animation(config, times, iterates, temperatures, path):
    if isinstance(config, dict):
        config = Configuration(config)
    grid = make_grid(config)
    shape = (len(grid[0]), len(grid[1]))
    X, Y = np.meshgrid(*grid)

    def render_frame(i, plot):
        # This exploits the special structure of the finite element space
        data = np.reshape(iterates[i], shape)
        plot[0].remove()
        plot[0] = ax.plot_surface(X, Y, data, cmap='magma', vmin=0, vmax=105)
        plt.title('t = %ds, potato temperature = %d°C' % (times[i], temperatures[i]))

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    plot = [
        ax.plot_surface(
            X, Y, np.zeros(shape), color='0.75', rstride=1, cstride=1, vmin=0, vmax=105
        )
    ]
    ax.set_zlim(0, 125)

    fps = 15

    animation = matplotlib.animation.FuncAnimation(
        fig,
        func=render_frame,
        fargs=(plot,),
        frames=np.arange(0, len(iterates), 16),
        interval=1000 / fps,
        blit=False,
    )

    if path.endswith('.gif'):
        # Note: GIF export may fail when there are too many iterates.
        animation.save(path, writer='imagemagick', fps=fps)
    elif path.endswith('.mp4'):
        animation.save(path + '.mp4', writer='ffmpeg', fps=fps)
    elif path is not None:
        warnings.warn(
            'Extension of file "%s" is not supported for saving animations.' % path
        )
