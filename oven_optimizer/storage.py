import json
import os
import pathlib

import numpy


# Taken from https://stackoverflow.com/questions/57269741/typeerror-object-of-type-ndarray-is-not-json-serializable  # noqa: E501
class NumpyEncoder(json.JSONEncoder):
    ''' Special json encoder for numpy types '''
    def default(self, obj):
        if isinstance(
            obj,
            (
                numpy.int_,
                numpy.intc,
                numpy.intp,
                numpy.int8,
                numpy.int16,
                numpy.int32,
                numpy.int64,
                numpy.uint8,
                numpy.uint16,
                numpy.uint32,
                numpy.uint64,
            ),
        ):
            return int(obj)
        elif isinstance(
            obj, (numpy.float_, numpy.float16, numpy.float32, numpy.float64)
        ):
            return float(obj)
        elif isinstance(obj, (numpy.ndarray,)):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def store_data(path, **kwargs):
    '''Stores data as JSON for easy interoperability with other tools.
    Not recommended for large amounts of data due to plaintext encoding.
    '''
    directory = str(pathlib.Path(path).parent)
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(path, 'w') as file:
        json.dump(kwargs, file, cls=NumpyEncoder)


def load_data(path):
    with open(path, 'r') as file:
        return json.load(file)
