''' This script file solves the "forward simulation problem" that
    is described in the section "scenario 1" of the readme.
'''
import numpy as np
import scipy.interpolate

from oven_optimizer.assembler import (
    assemble_all_discrete_systems,
    make_constant_function,
)
import oven_optimizer.configuration_manager
import oven_optimizer.euler_schema
from oven_optimizer.local_logging import logger
import oven_optimizer.storage

from visualize import visualize_results


def run_scenario():
    ''' Run the scenario from scratch. '''
    # First, we load all relevant configuration parameters from a file.
    # This makes for a better decoupling of code logic and problem definition.
    config = oven_optimizer.configuration_manager.read('config.yaml')

    # Assemble all the discrete systems for carrying out the simulation.
    # We can do this here already because the problem is linear.
    discrete_systems = assemble_all_discrete_systems(config)

    # Set up the controller which maps an input state to a
    # finite element function describing the power along the boundary.
    controller = make_controller(config, discrete_systems)

    # We define an implicit Euler schema based on our discrete systems.
    euler_schema = make_euler_schema(config, discrete_systems, controller)

    # We run the schema with a zero initial start value.
    # For the sake of having more interesting visualizations later on,
    # we retrieve all the iterates and not just the very last one.
    initial_state = make_constant_function(
        function_space=discrete_systems.constant_one, constant=0
    )
    times, states = euler_schema.run(
        initial_iterate=initial_state.coefficients,
        stop_time=config.get('problem/stop_time'),
        return_only_last_iterate=False,
    )
    logger.debug('Finished running Euler schema.')

    # Extract the relevant result information.
    temperatures = [discrete_systems.get_potato_temperature(state) for state in states]
    energy_consumption = discrete_systems.get_energy_consumption(
        times, controller.history
    )

    # Store the results.
    oven_optimizer.storage.store_data(
        'output/results.json',
        config=config.as_dict(),
        temperatures=temperatures,
        energy_consumption=energy_consumption,
        times=times,
        states=states,
        controls=controller.history,
    )

    # On this occasion, we also create some graphics for result visualization.
    # (Usually, it would make sense to have the report generation in a separate
    # location, but here it is convenient enough to just do it right away.)
    logger.info('Reached the final temperature: %.2f', temperatures[-1])
    visualize_results('output/results.json')


def make_controller(config, discrete_systems):
    action_map = get_action_map(config)

    def get_control(state):
        # Evaluate average temperature across potato.
        temperature = discrete_systems.get_potato_temperature(state)

        # Infer the functional representation of the right hand side.
        action = action_map(temperature) / 1000
        # logger.debug('Measured temperature %.2f. Taking action %.2f.',
        #     temperature, action)
        control = make_constant_function(
            function_space=discrete_systems.constant_one, constant=action
        )
        get_control.history.append(np.copy(control.coefficients))
        return control.coefficients

    get_control.history = []
    return get_control


def get_action_map(config):
    grid = config.get('action_map/grid')
    values = config.get('action_map/values')
    action_map = scipy.interpolate.interp1d(
        grid,
        values,
        kind='linear',
        bounds_error=False,
        fill_value=(values[0], values[-1]),
    )
    return action_map


def make_euler_schema(config, discrete_systems, controller):
    def right_hand_side_generator(state):
        control = controller(state)
        right_hand_side = np.matmul(discrete_systems.boundary_mass_matrix, control)
        return right_hand_side

    euler_schema = oven_optimizer.euler_schema.ImplicitEulerSchema(
        config.get('euler_schema/time_step'),
        discrete_systems.mass_matrix,
        -discrete_systems.laplace_matrix,
        right_hand_side_generator,
    )
    return euler_schema


def get_energy_consumption(discrete_systems, times, controls):
    ''' Calculate the cumulative integral over the domain boundary. '''
    time_deltas = np.diff(times)
    integrals = [0]
    for time_delta, control in zip(time_deltas, controls[: len(time_deltas)]):
        integrals.append(
            integrals[-1]
            + time_delta
            * np.dot(control, np.matmul(discrete_systems.boundary_mass_matrix, control))
        )
    return integrals


if __name__ == '__main__':
    run_scenario()
    logger.info('Done.')
