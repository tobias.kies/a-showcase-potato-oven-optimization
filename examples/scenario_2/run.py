''' This script file solves the optimal control problem that
    is described in the section "scenario 2" of the readme.
'''
import numpy as np

from oven_optimizer.assembler import (
    assemble_all_discrete_systems,
    make_constant_function,
)
import oven_optimizer.configuration_manager
from oven_optimizer.heating_control_problem import HeatingControlProblemDiscretization
from oven_optimizer.local_logging import logger
from oven_optimizer.projected_gradient_method import ProjectedGradientMethod

from visualize import visualize_results


def run_scenario():
    # Do similar preparations as in scenario 1.
    config = oven_optimizer.configuration_manager.read('config.yaml')
    discrete_systems = assemble_all_discrete_systems(config)

    # Initialize the optimal control problem.
    ocp = make_optimal_control_problem(config, discrete_systems)

    # Set up projected gradient method as solver.
    solver = ProjectedGradientMethod(
        max_num_iterations=config.get('projection_method/max_num_iterations'),
        store_history=True,
    )

    # Apply solver.
    box_projection = make_box_projection(config)
    initial_iterate = make_initial_control_trajectory(config, discrete_systems)
    boundary_controls = solver.run(
        ocp.get_cost, ocp.get_derivative, box_projection, initial_iterate
    )

    # Extract relevant quantities.
    times, states = ocp.get_last_state_trajectory()
    embedded_controls = [
        discrete_systems.embed_from_boundary(boundary_controls[i, :])
        for i in range(np.shape(boundary_controls)[0])
    ]
    temperatures = [discrete_systems.get_potato_temperature(state) for state in states]
    energy_consumption = discrete_systems.get_energy_consumption(
        times, embedded_controls
    )

    # Store results.
    oven_optimizer.storage.store_data(
        'output/results.json',
        config=config.as_dict(),
        temperatures=temperatures,
        energy_consumption=energy_consumption,
        times=times,
        states=states,
        controls=embedded_controls,
    )

    # Visualize results.
    logger.info('Reached the final temperature: %.2f', temperatures[-1])
    visualize_results('output/results.json')


def make_optimal_control_problem(config, discrete_systems):
    initial_state = make_constant_function(
        function_space=discrete_systems.constant_one, constant=0
    )
    target_state = make_constant_function(
        function_space=discrete_systems.constant_one,
        constant=config.get('problem/target_temperature'),
    )
    ocp = HeatingControlProblemDiscretization(
        initial_state=initial_state,
        target_state=target_state,
        regularization=config.get('problem/regularization'),
        end_time=config.get('problem/stop_time'),
        discrete_systems=discrete_systems,
        time_step=config.get('euler_schema/time_step'),
    )
    return ocp


def make_box_projection(config):
    min_control = config.get('problem/minimum_heat_control')
    max_control = config.get('problem/maximum_heat_control')

    def box_projection(x):
        return np.minimum(np.maximum(x, min_control), max_control)

    return box_projection


def make_initial_control_trajectory(config, discrete_systems):
    constant_zero = make_constant_function(
        function_space=discrete_systems.constant_one, constant=0
    )
    zero_boundary_coefficients = discrete_systems.reduce_to_boundary(
        constant_zero.coefficients
    )
    num_time_points = len(make_time_grid(config))
    control_trajectory = [zero_boundary_coefficients for _ in range(num_time_points)]
    return np.array(control_trajectory)


def make_time_grid(config):
    time_step = config.get('euler_schema/time_step')
    stop_time = config.get('problem/stop_time')
    grid = list(np.arange(0, stop_time, time_step))
    if grid[-1] != stop_time:
        grid.append(stop_time)
    return grid


if __name__ == '__main__':
    run_scenario()
