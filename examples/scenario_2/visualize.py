import os

import oven_optimizer.configuration_manager
import oven_optimizer.storage
import oven_optimizer.visualization

OUTPUT_DIRECTORY = 'output/img'


def visualize_results(path):
    data = oven_optimizer.storage.load_data(path)
    if not os.path.exists(OUTPUT_DIRECTORY):
        os.makedirs(OUTPUT_DIRECTORY)

    oven_optimizer.visualization.make_line_plot(
        x=data['times'],
        y=data['temperatures'],
        path=make_path('temperatures'),
        title='Temperatures. Final temperature: %.2f' % data['temperatures'][-1],
        x_label='time (s)',
        y_label='temperature (°C)',
    )

    oven_optimizer.visualization.make_line_plot(
        x=data['times'],
        y=data['energy_consumption'],
        path=make_path('energy_consumption'),
        title='Cumulative cost. Total cost: %.2f' % data['energy_consumption'][-2],
        x_label='time (s)',
        y_label='cost',
    )

    oven_optimizer.visualization.make_animation(
        config=data['config'],
        times=data['times'],
        iterates=data['states'],
        temperatures=data['temperatures'],
        path=make_path('state_trajectory', ext='.gif'),
    )


def make_path(name, ext='.png'):
    return OUTPUT_DIRECTORY + '/' + name + ext


if __name__ == '__main__':
    visualize_results('output/results.json')
