#!/bin/bash

set -e

# Make sure that all necessary dependencies are installed.
apt-get install -y python3

# Install the fesuite python module.
cd fesuite
./install.sh
cd ..

# Install the oven_optimizer python module.
pip install -e .

# Make plausibility checks for installation success.

# Make plausibility tests.
if [ $(pip list | grep "oven-optimizer " | wc -l) == "0" ]; then
    echo "Plausibility check failed: Package does not seem to be installed."
    exit 1
fi
echo "Checking if package import works..."
python -c "import oven_optimizer;"
echo "Installation finished successfully."
