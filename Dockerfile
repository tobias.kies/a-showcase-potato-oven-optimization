# We are using some python packages which are not readily
# avaible within alpine builds. Hence, we use the buster image.
FROM python:3.7-buster

RUN apt-get update && apt-get install -y bash build-essential

COPY oven_optimizer /app/oven_optimizer
COPY fesuite /app/fesuite
COPY examples /app/examples
COPY tests /app/tests
COPY install.sh /app/install.sh
COPY setup.py /app/setup.py

WORKDIR /app
RUN chmod +x install.sh && ./install.sh
