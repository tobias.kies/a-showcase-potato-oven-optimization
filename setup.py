from setuptools import setup, find_packages

EXTENSION_NAME = 'oven_optimizer'
DESCRIPTION = '''
A light-weight optimization library that comes along
with the 'Potato Oven Optimization' showcase.
'''
VERSION = '0.0.1'

AUTHOR = 'Tobias Kies'
AUTHOR_EMAIL = 'tobias.kies@gmx.de'
URL = 'https://gitlab.com/tobias.kies/a-showcase-potato-oven-optimization'

setup(
    name=EXTENSION_NAME,
    description=DESCRIPTION,
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    url=URL,
    packages=find_packages(),
    install_requires=[
        'pyyaml>=5.4.1',
        'numpy>=1.20.1',
        'scipy>=1.6.1',
        'seaborn>=0.11.1',
    ],
    python_requires='>=3',
)
