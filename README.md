# A Showcase: Potato Oven Optimization

This fun little project is a playground to showcase various techniques related to mathematical modeling, numerical optimization, and programming in general.
It mostly has an educational purpose, but it also serves as a collection of snippets and thoughts which are useful in more serious scenarios as well.

![Temperature distribution with respect to time](https://gitlab.com/tobias.kies/a-showcase-potato-oven-optimization/-/raw/master/examples/scenario_1/output/img/state_trajectory.gif)

## What is this project about? What interesting things are there to be seen?
Imagine you are working for the world's best manufacturer of potato ovens.
Since competition in this industry is extremely harsh, continuous improvements are of paramount importance in order to stay at the top.
You are going to help with this!

From a mathematical and numerical perspective, you are going to see:
* a simple heat equation model for describing how temperature evolves in an oven
* how to solve the resulting parabolic partial differential equation numerically using finite element discretizations
* an optimal control problem (for reaching a target temperature state with as little energy as possible)
* how to solve an optimal control problem numerically using the projected gradient method

From a more pragmatic and programmatical perspective, you can also learn about:
* how finite element discretizations are handled in C++ (refer to the submodule [FeSuite](https://gitlab.com/tobias.kies/fesuite))
* how to call C++ code from python (also part of [FeSuite](https://gitlab.com/tobias.kies/fesuite))
* how to make Python modules installable via pip (refer to `install.sh` and `setup.py`)
* how to write unit tests for python (refer to `tests/` which contains at least a few of those, but surely not enough yet)
* how to ensure Python code quality continuously by setting up a CI/CD pipeline which runs the PEP8 linter `flake8` and executes tests via `pytest` (refer to `.gitlab-ci.yml`)
* how to create a reference environment using Docker images (refer to `Dockerfile` and `.gitlab-ci.yml` - no more excuses "but it did work on my system"!)

Of course, it is virtually impossible to cover all of these subjects at the full length that they truly deserve.
However, where appropriate, we will try to mention alternative approaches and references to related literature.
In that sense, this project helps to find an entry point into the above-mentioned topics.

## Repository Folder Structure
* `oven_optimizer/` contains functions that are of general use for the optimization problems in this repository (and may even be useful for other applications as well)
* `fesuite/` is a submodule and points to the source of the FeSuite python package which we use for our finite element discretizations
* `examples/` contains the very specific code (and also some output data) for the scenarios that we are going to discuss further down below
* `tests/` contains some unit tests (but not nearly enough yet)

## Installation Notes
So far, the installation is only designed to run under Linux environments.
The python parts also readily apply to Windows, but this package also uses the module [fe_suite](https://gitlab.com/tobias.kies/fesuite) which requires compilation of C++ sources and hence deserves special attention.

If you are running Ubuntu Linux (or something comparable) and have `python3` installed already, then you should be ready to go by checking out the `fe_suite` submodule
```sh
git submodule init fe_suite
git submodule update
```
and then running the install script
```sh
sudo ./install.sh
```

Optionally, you can also install everything into a separate virtual environment by running the commands
```sh
virtualenv .env --python=python3
source .env/bin/activate
```
before calling the install script (again assuming that you have `python3` installed already).

> **Note:**
For the developer's convenience, this script installs the python sources in "editable" mode.
This means, changes to `*.py` files take effect immediately!
If you make changes to the C code inside the `fe_suite` module, then you need to re-run the install script.

### Using Docker
If you do not want to install the code on your system, you can also use Docker instead.
Using e.g. [Docker for Windows](https://www.docker.com/products/docker-desktop) it should even be possible to execute the examples from this repository in a Windows environment.

For example, if you have docker installed and want to run the first scenario, you can issue the following commands:

```sh
docker run -it registry.gitlab.com/tobias.kies/a-showcase-potato-oven-optimization:latest /bin/bash
cd examples/scenario_1
python run.py
```

When the script execution is done, you can extract the result images using `docker cp`.

## Acknowledgement
Pretty much all of the scientific code in this repository was designed and written by myself (unless explicitly stated otherwise).

However, the mathematical presentation in the scenarios below benefits a lot from the introduction to optimal control problems in the book "Optimal Control of Partial Differential Equations" by Fredi Tröltzsch.

Also, the submodule FeSuite is heavily inspired by the code base of the [DUNE project](https://dune-project.org/) with which I worked a lot during my times as a PhD student.

## Scenarios
Check out these wiki pages for concrete applications:
* [Scenario 1: Forward-Simulation - Do we have to actually build an oven?](https://gitlab.com/tobias.kies/a-showcase-potato-oven-optimization/-/wikis/Scenario-1)
* [Scenario 2: Solving an Optimal Control Problem - What is the best solution possible?](https://gitlab.com/tobias.kies/a-showcase-potato-oven-optimization/-/wikis/Scenario-2)
